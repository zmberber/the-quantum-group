\section{The quantum group}
Recall that the \emph{universal enveloping algebra} $\envel(\lie{g})$ of a Lie algebra $\lie{g}$ is an associative unital $k$-algebra with a linear map $\envel \mdef \lie{g} \to \envel(\lie{g})$, defined by the following universal property:
For any linear map $\varphi \mdef \lie{g} \to A$ such that $\varphi(\liebr{a}{b}) = \varphi(a)\varphi(b) - \varphi(b)\varphi(a)$ for all $a,b \in \lie{g}$, where $A$ is an associative unital $k$-algebra, there exists a unique $k$-algebra morphism $\psi \mdef \envel(\lie{g}) \to A$ such that $\phi = \psi \comp \envel$
\begin{equation*}
  \begin{tikzcd}
    \lie{g} \ar[d,"\envel"'] \ar[r,"\phi"] & A \\
    \envel(\lie{g}) \ar[ur,"\psi"'] & .
  \end{tikzcd}
\end{equation*}
We then have a correspondence between $\lie{g}$-representations and $\envel(\lie{g})$-representations.
A concrete construction can be
\begin{equation*}
  T(\lie{g}) / \gen{a\tensor b - b\tensor a - [a,b]} \;,
\end{equation*}
so that concretely for $\lie{g} = \slt$, we then have $\envel(\slt)$ as the associative unital $k$-algebra over generators $e,f,h$, with the relations
\begin{align}
  he-eh &= 2e \tag{A1}\label{hooli2}  \\
  hf-fh &= -2f \tag{A2}\label{hooli3}  \\ 
  ef-fe &= h \;. \tag{A3}\label{hooli4}
\end{align}
We will now modify this algebra into a ``quantum version''.
\begin{definition}[quantum group]
  The \emph{quantum group} $\envel_q(\slt)$ is defined as the associative unital $\overline{k(q)}$-algebra with generators $K,K^{-1},E,F$ and relations
  \begin{align}
    KK^{-1} &= K^{-1}K = 1 \tag{B0}\label{juan} \\
    KE &= q^2EK \tag{B1}\label{juan2} \\
    KF &= q^{-2}FK  \tag{B2}\label{juan3}\\
    EF-FE &= \frac{K-K^{-1}}{q-q^{-1}} \; . \tag{B3}\label{juan4}
  \end{align}
\end{definition}

\emph{From now on, we will work over the field $\overline{k(q)}$ when we are in the context of $\envel_q(\slt)$.}

We can see how \cref{hooli2} is similar to \cref{juan2}, \cref{hooli3} to \cref{juan3} and \cref{hooli4} to \cref{juan4}.
The definition makes sense in the following way.
For an eigenvector $w \in W_n$ of $h$ of an $\envel(\slt)$-module $W$ with eigenvalue $n$, we have (as seen in the picture)
\begin{align*}
  he.w &= (n+2)e.w\\
  hf.w &= (n-2)f.w \;.
\end{align*}
For a eigenvector $v \in V_{q^n}$ of $K$ of an $\envel_q(\slt)$-module $V$ with eigenvalue $q^n$, we have
\begin{align*}
  KEv &=  q^{n+2}Ev  \\
  KFv &= q^{n-2}Ev \;.
\end{align*}

For a weight vector $w$ of weight $q^n$, we see that
\begin{equation*}
  \begin{split}
    (EF-FE)v
    &= \frac{K-K^{-1}}{q-q^{-1}}w  \\
    &= \frac{q^n-q^{-n}}{q-q^{-1}}w \\
    &= [n]w \; ,
  \end{split}
\end{equation*}
which is reminiscent of $(ef-fe)w = nw$ for a weight vector $w$ of $\envel(\slt)$.
In fact, if we evaluate $q=1$, then we do get that relation with $\res{[n]}{q=1} = n$, and similarly we also get $\res{[n]!}{q=1} = n!$.
This inspires the notion of an ``$H$''.

\begin{definition}
  We define
  \begin{equation*}
    H \defeq EF - FE = \frac{K-K^{-1}}{q-q^{-1}} \in \envel_q(\slt) \;.
  \end{equation*}
\end{definition}

We want to see that \cref{figga} with integers replaced by quantum integers and operators replaced by their quantum operators makes sense.
Let $V$ be an irreducible, finite-dimensional $U_v(\slt)$-module.
Quite analogously to the proof for characterizing finite-dimensional irreducible $\slt$-representations, we see that there must exist an eigenvector $v$ of $K$ such that $Ev = 0$, with eigenvalue $\lambda$.
We then also calculate that $KF^iv = (\lambda / q^{2i})F^iv$.
For some $d$, we must have $F^dv \neq 0$ but $F^{d+1}v = 0$.
Furthermore, we calculate
\begin{equation*}
  \begin{split}
    0
    &= EF^{d+1}v  \\
    &= (F^{d+1}
    E + [d+1]F^d[K;-d]v  \\
    &= [d+1]F^d[K;-d]v  \;,
  \end{split}
\end{equation*}
where we used $EF^{i} = F^{i}E + [i]F^{i-1}[K;1-i]$ with $[K;j] \defeq \frac{Kq^{j} - K^{-1}q^{-j}}{q-q^{-1}}$.
Therefore $Kv = q^d$ ir $Kv = -q^d$.
Let us look at the case $Kv = q^d$.
We then must have $EF^iv = [i]F^{i-1}[d+1-i]v$ and also $Hv = [d]v$.

From here we can finally construct an analogous diagram to \cref{figga}, as seen in \cref{fig2}.

\begin{figure}[h!]
  \begin{equation*}
    \adjustbox{scale=0.8,center}{%
      \begin{tikzcd}[cells={nodes={}}]
        v
        \ar[loop above,"\lbrack d\rbrack \mult",color=blue]
        \ar[r,bend right,"\lbrack 1 \rbrack \mult"',color=red,pos=0.7] &
        \ar[l,bend right,"\lbrack d\rbrack \mult"',color=green,pos=0.3]
        \frac{1}{\lbrack 1 \rbrack !}F^1.v
        \ar[loop above,"\lbrack d-2\rbrack\mult",color=blue]
        \ar[r,bend right,"\lbrack 2 \rbrack \mult"',color=red,pos=0.7] &
        \ar[l,bend right,"\lbrack d-1\rbrack\mult"',color=green]
        \frac{1}{\lbrack 2 \rbrack!}F^2.v
        \ar[loop above,"\lbrack d-4\rbrack\mult",color=blue]
        \ar[r,bend right,"\lbrack 3 \rbrack \mult"',color=red] &
        \ar[l,bend right,"\lbrack d-2\rbrack\mult"',color=green,pos=0.6]
        \mathrlap{\phantom{\frac{1}{[2]!}F^2.v}}\dots
        \ar[r,bend right,"\lbrack d-2\rbrack\mult"',color=red,pos=0.6] &
        \ar[l,bend right,"\lbrack 3 \rbrack \mult"',color=green]
        \frac{1}{\lbrack d-2\rbrack!}F^{d-2}.v
        \ar[loop above,"-\lbrack d-4\rbrack\mult",color=blue]
        \ar[r,bend right,"\lbrack d-1\rbrack\mult"',color=red] &
        \ar[l,bend right,"\lbrack 2 \rbrack \mult"',color=green]
        \frac{1}{\lbrack d-1\rbrack!}F^{d-1}.v
        \ar[loop above,"-\lbrack d-2\rbrack\mult",color=blue]
        \ar[r,bend right,"\lbrack d\rbrack\mult"',color=red] &
        \ar[l,bend right,"\lbrack 1\rbrack \mult"',color=green]
        \frac{1}{\lbrack d\rbrack !}F^d.v
        \ar[loop above,"\lbrack -d\rbrack \mult",color=blue]
      \end{tikzcd}}
  \end{equation*}
  \caption{Color code: action by {\color{green}$E$}, {\color{red}$F$} and {\color{blue}$H$}, respectively}\label{fig2}
\end{figure}

% So now we have the relations
% \begin{equation*}
%   \begin{split}
%     (HE-EH) v
%     % &= \frac{q^2EK-q^{-2}EK^{-1}}{q-q^{-1}} - \frac{q^{-2}KE - q^2K^{-1}E}{q-q^{-1}}  \\
%     % &= \frac{q^2EK-q^{-2}EK^{-1}}{q-q^{-1}} - \frac{EK-EK^{-1}}{q-q^{-1}}  \\
%     % &= (q^2-q^{-2}-1)EH
%     % &= \frac{KE-K^{-1}E}{q-q^{-1}} - \frac{q^{-2}KE - q^2K^{-1}E}{q-q^{-1}}  \\
%     &= [n+2]Ev - [n]Ev \\
%     &= 
%   \end{split}
% \end{equation*}


% If $V$ is an irreducible $\envel_q(\slt)$-module of dimension $d+1$, we then get, with the same explanation as for $\envel(\slt)$, that if $v$ is a weight vector of weight

% Blablabla changeah
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "master"
%%% End:
